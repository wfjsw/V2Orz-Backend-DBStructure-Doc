# thread.sha256.json 示例

```javascript
{
    "thread_meta": {
        "title": "帖子标题",
        "author": "DEADBEEF",  //作者的pgp-key id
    },
    "main_content": {
        "hashkey": "a000000...",  //指示存储正文的content-blob key
        "update_time": 0  //指示发布时间, 格式待定
    },
    "replys": [
        {
            "author": "DEADBEEF",  //作者的pgp-key id
            "hashkey": "a000000...",  //指示存储该回复的content-blob key
            "update_time": 0  //指示发布时间
        },
        {
            "author": "DEADBEEF",  //作者的pgp-key id
            "hashkey": "a000000...",  //指示存储该回复的content-blob key
            "update_time": 0  //指示发布时间
        },
        //etc...
    ],
    "tags": [
        {   
            "author": "DEADBEEF",  //作者的pgp-key id
            "tag": "useful",  //标签的内容
            "signature": "".  //标签的pgp签名（可选）
            "update_time": 0  //指示发布时间
        }
        //etc...
    ],
}
```