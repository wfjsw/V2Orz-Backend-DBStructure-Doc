# 结构示意图

```
db/   --数据库根目录
   indexes/          --用于存储索引信息
           node1/    --这里名字自定义，用于区分节点，以nodes.json指向为准
                 threads.json        --存储该节点下所有topic的索引
                 thread.deadbef.json --存储topic正文sha256末7位为deadbef的帖子信息
           (以此类推什么的...)
   contents/         --用于存储用户的发帖内容(blobs)
            a0/        --取sha256的前2位
               a00000... (左边应该是个sha256) --blob存储
            (以此类推什么的...)
   nodes.json        --列举节点列表
   nodes.json.sha256 --用于检测是否需要更新 
   config.json  --存储Tahoe-LAFS地址等连接配置(应当通过手段预先分发)
```