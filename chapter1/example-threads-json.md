# threads.json 示例

```javascript
{
    "node_metadata": {
        "uniqueid": "Python",
        "description": "超长简介",
        "logo_uri": "标题logo",
        "links_hashkey": "a00000...." //友链之类的，内容在contents，这里只填sha256 key
    },
    "archive_file": "threads.archive.json", //存储较老帖子
    "threads_list": {  //这里只存200个，其余都丢到archive 原则是新的在下面
        "70acb3d": {
            "title": "帖子标题",
            "replys: 15 //回复的数目
            },
        //etc...
    }
}
```