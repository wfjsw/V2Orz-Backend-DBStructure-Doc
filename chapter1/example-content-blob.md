# content blob 示例

**注意: blob文件的文件名应总是blob中内容的SHA256**

对于存储友链的blob, 仅包含有友链内容的一段html(仅文本或超链接)  
实际案例: (摘自V2EX)
```html
<strong class="green">推荐学习书目</strong>
<div></div>
<span>›</span> <a href="http://learnpythonthehardway.org/book/" target="_blank">Learn Python the Hard Way</a>
<div></div>
<span>›</span> <a href="http://www.amazon.cn/mn/detailApp/ref=as_li_ss_tl?_encoding=UTF8&amp;tag=v06-23&amp;linkCode=as2&amp;asin=B004TUJ7A6&amp;camp=536&amp;creative=3132&amp;creativeASIN=B004TUJ7A6">Python 学习手册</a><img class="jorzlngygtvblpzrcarc" src="//www.assoc-amazon.cn/e/ir?t=&amp;l=as2&amp;o=28&amp;a=B004TUJ7A6" alt="" style="border:none !important; margin:0px !important;" width="1" height="1" border="0">
<div></div>
<span>›</span> <a href="http://www.amazon.cn/mn/detailApp/ref=as_li_ss_tl?_encoding=UTF8&amp;tag=v06-23&amp;linkCode=as2&amp;asin=B003LPO4KS&amp;camp=536&amp;creative=3132&amp;creativeASIN=B003LPO4KS">Python Cookbook</a><img class="jorzlngygtvblpzrcarc" src="//www.assoc-amazon.cn/e/ir?t=&amp;l=as2&amp;o=28&amp;a=B003LPO4KS" alt="" style="border:none !important; margin:0px !important;" width="1" height="1" border="0">
<div></div>
<span>›</span> <a href="http://www.amazon.cn/mn/detailApp/ref=as_li_ss_tl?_encoding=UTF8&amp;tag=v06-23&amp;linkCode=as2&amp;asin=B003TSBAMM&amp;camp=536&amp;creative=3132&amp;creativeASIN=B003TSBAMM">Python 基础教程</a><img class="jorzlngygtvblpzrcarc" src="//www.assoc-amazon.cn/e/ir?t=v06-23&amp;l=as2&amp;o=28&amp;a=B003TSBAMM" alt="" style="border:none !important; margin:0px !important;" width="1" height="1" border="0">
```


---


对于存储用户发布内容(UGC)的blob, 一律使用PGP inline-signed的markdown  
实际案例:
```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

**这**是一个示例*消息*。
![](链接) 是可用的。

-----BEGIN PGP SIGNATURE-----
Version: Keybase OpenPGP v2.0.47
Comment: https://keybase.io/crypto

wsBcBAABCgAGBQJWEI+MAAoJEDFWzGJ+CS41gUkH/ieqhUIJW7qTqpahvfWgHp5b
QKDWyr38p95Nm8u9ODETsHh5uUbhX905HyIU/rQV2nAYkvWlW8e2E8kBG+1Frrzv
guMOD2QE/6hu6mGe+AmNJcqtoqiz3jsOqOWgQPhXwjKUAZwYxMDaQxXg71yYNtFc
US+TlxKF0jOcQq9r2J6bg2y1VGPwzQ6lGb/btcIPTzEwix/SdjtNaHf5HOKTxcGw
zmVcZhUgX713T6oc0SL8L7eDseYu0dezPsFFHz+b71dl+rABRhf4Rut0PJMjp+X8
XFD4e1Vitq4C5PCm/axMDxe/Lh+QAWHNcCDObigBWZ8JafF6t4NYtHbLlMTJKR4=
=Q1D0
-----END PGP SIGNATURE-----

```