# nodes.json 示例

```javascript
{
    "enabled_nodes": ["Geek", "Apple", "城市"],
    "nodes": {
        "Geek": {   //大分类
            "程序员": "indexes/programmers", //指向node所在目录的小节点
            "Python": "indexes/python",
            "Android": "indexes/android"
            //etc...
        },
        "Apple": {
            "Mac OS X": "indexes/macosx"
            //etc...
        }
    }
}
```